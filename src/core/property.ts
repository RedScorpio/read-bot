import { Observable, Subscription, Subject } from 'rxjs';

export class Property<T> {
  private subject: Subject<T>;
  private observable: Observable<T>;

  constructor() {
    this.subject = new Subject();
    this.observable = this.subject.asObservable();
  }

  public next(value: T): void {
    this.subject.next(value);
  }

  public subscribe(subscriber: (value: T) => void): Subscription {
    return this.observable.subscribe(subscriber);
  }
}
