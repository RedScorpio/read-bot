'use strict';

const Second = 1000;
const Minute = 60 * Second;
const Hour = 60 * Minute;
const Day = 24 * Hour;

function anyDateToLong(date: string|Date|number): number {
  if (typeof (date) === 'string') {
    date = new Date(date).getTime();
  } else if (date instanceof Date) {
    date = date.getTime();
  }
  return date;
}

export function formatDate(d: Date): string {
  return `${d.getHours()}:${d.getMinutes()} ${d.getDate()}.${d.getMonth()}.${d.getFullYear()}`;
}

export function formatSimpleDate(date: string|Date|number): string {
  let time = anyDateToLong(date);
  const now = Date.now();
  if (now - 10 * Second < time) {
    return 'a moment ago';
  } else if (now - Minute < time) {
    return 'within the last minute';
  } else if (now - 2 * Minute < time) {
    return 'a minute ago';
  } else if (now - Hour < time) {
    return Math.round((now - time) / Minute) + ' minutes ago';
  } else if (now - Day < time) {
    return Math.round((now - time) / Hour) + ' hours ago';
  } else if (now - 2 * Day < time) {
    return 'yesterday';
  } else {
    return formatDate(new Date(time));
  }
}
