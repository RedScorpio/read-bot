'use strict';

const Env = process.env;

export const Prefs = {
  SlackToken: 'slackToken',
  SlackName: 'slackName',
  AmazonKeyId: 'amazonKeyId',
  AmazonSecret: 'amazonSecret',
  AmazonRegion: 'amazonRegion',
  Voice: 'voice',
  AutoConnect: 'autoConnect'
};

interface CustomStorage {
  setItem(key: string, value: number|string);
  clear();
  getItem(key: string): any;
  removeItem(key: string);
}

class TemporaryStorage implements CustomStorage {

  private store;

  constructor() {
    this.clear();
  }

  setItem(key: string, value: number|string): void {
    this.store[key] = value;
  }

  clear(): void {
    this.store = {};
  }

  getItem(key: string): any {
    return this.store[key];
  }

  removeItem(key: string): void {
    delete this.store[key];
  }

}

export const storage: CustomStorage = (Env.Private === 'true' || !localStorage) ? new TemporaryStorage() : localStorage;

for (const key in Prefs) {
  if (Prefs.hasOwnProperty(key) && Env.hasOwnProperty(key)) {
    storage.setItem(Prefs[key], Env[key]);
  }
}