import { should as shouldInit } from 'chai';
import { SlackClient } from '../modules/slack/client/slack-service';
import { Subscription } from 'rxjs';
import { SlackMessage } from '../modules/slack/client/slack-message';
import { SlackUser } from '../modules/slack/client/slack-user';
import { storage, Prefs } from '../core/storage';

const should = shouldInit();

describe('Slack Models', function () {

  let slackClient: SlackClient;
  let subscription: Subscription|null = null;

  beforeEach(function () {
    slackClient = SlackClient.newInstance();
  });

  afterEach(function () {
    if (subscription) {
      subscription.unsubscribe();
      subscription = null;
    }
    storage.clear();
  });

  describe('Message Tests', function () {
    it('should correctly interpret a message (user list missing)', function (done) {
      // Listen for messages
      subscription = slackClient.messagesObservable.subscribe(messages => {
        messages.should.have.lengthOf(1);
        const message: SlackMessage = messages[0];
        message.date.should.eql(new Date(1358878749.000002 * 1000));
        message.text.should.equal('Hello from <@U023BECGF>.');
        // User not parsed and id left out
        message.htmlText.should.equal(message.text);
        message.speechText.should.equal(message.text);
        should.not.exist(message.sender);
        done();
      });
      // Emit a message
      slackClient.onMessageReceived(require('./slack-mocks/simple-message.json'));
    });

    it('should correctly interpret a message (with user list)', function (done) {
      // Listen for messages
      subscription = slackClient.messagesObservable.subscribe(messages => {
        messages.should.have.lengthOf(1);
        const message: SlackMessage = messages[0];
        message.date.should.eql(new Date(1358878749.000002 * 1000));
        message.text.should.equal('Hello from <@U023BECGF>.');
        message.htmlText.should.equal('Hello from <i>@bobby</i>.');
        message.speechText.should.equal('Hello from Bobby Tables.');
        should.exist(message.sender);
        message.sender.id.should.equal('U023BECGF');
        done();
      });
      // Emit a user list and message
      slackClient.onUsersReceived(require('./slack-mocks/user-list.json'));
      slackClient.onMessageReceived(require('./slack-mocks/simple-message.json'));
    });

    it('should filter multiple messages by the correct name', function (done) {
      // Set preferred name and emit user list.
      storage.setItem(Prefs.SlackName, 'bobby');
      slackClient.onUsersReceived(require('./slack-mocks/user-list.json'));
      // Listen for messages
      subscription = slackClient.messagesObservable.subscribe(messages => {
        messages.length.should.be.lessThan(3);
        const message: SlackMessage = messages[0];
        message.date.should.eql(new Date(1358878749.000002 * 1000));
        message.text.should.equal('Hello from <@U023BECGF>.');
        message.htmlText.should.equal('Hello from <i>@bobby</i>.');
        message.speechText.should.equal('Hello from Bobby Tables.');
        should.exist(message.sender);
        message.sender.id.should.equal('U023BECGF');
        if (messages.length == 2) {
          done();
        }
      });
      // Emit multiple messages
      slackClient.onMessageReceived(require('./slack-mocks/simple-message.json'));
      slackClient.onMessageReceived(require('./slack-mocks/simple-message-2.json'));
      slackClient.onMessageReceived(require('./slack-mocks/simple-message-2.json'));
      slackClient.onMessageReceived(require('./slack-mocks/simple-message.json'));
    });

    it('should parse multiple names in message correctly', function (done) {
      // Listen for messages
      subscription = slackClient.messagesObservable.subscribe(messages => {
        messages.should.have.lengthOf(1);
        const message: SlackMessage = messages[0];
        message.date.should.eql(new Date(1358878749.000002 * 1000));
        message.text.should.equal('Hello from <@U023BECGD> to <@U023BECGF>.');
        message.htmlText.should.equal('Hello from <i>@robbie</i> to <i>@bobby</i>.');
        message.speechText.should.equal('Hello from Robbie Marbles to Bobby Tables.');
        should.exist(message.sender);
        message.sender.id.should.equal('U023BECGD');
        done();
      });
      // Emit a user list and message
      slackClient.onUsersReceived(require('./slack-mocks/user-list.json'));
      slackClient.onMessageReceived(require('./slack-mocks/simple-message-2.json'));
    });
  });

  describe('User Tests', function () {
    it('should correctly process a user list', function (done) {
      subscription = slackClient.usersObservable.subscribe(users => {
        users.should.have.lengthOf(2);
        const user: SlackUser = users[0];
        user.id.should.equal('U023BECGF');
        user.idReference.should.equal('<@U023BECGF>');
        user.name.should.equal('bobby');
        user.realName.should.equal('Bobby Tables');
        done();
      });
      slackClient.onUsersReceived(require('./slack-mocks/user-list.json'));
    });

    it('should correctly convert id to id reference', function () {
      const user = new SlackUser({
        id: 'identifier'
      });
      user.idReference.should.equal('<@identifier>');
    });
  });

});