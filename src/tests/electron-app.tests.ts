import { should } from 'chai';
const Application = require('spectron').Application;

should();
let app;

describe('Electron app tests', function () {
  this.timeout(10000);

  beforeEach(function () {
    app = new Application({
      path: process.env.RUN_PATH
    });
    return app.start();
  });

  afterEach(function () {
    if (app && app.isRunning()) {
      return app.stop();
    }
  });

  it('should be able to launch an app', function () {
    return app.client.getWindowCount()
      .then(count => {
        count.should.equal(1);
      });
  });
  it('should load index page and show slack component with connect button', function () {
    return app.client.getText('#slack-connect')
      .then(buttonText => {
        buttonText.should.equal('Connect');
      });
  });
});