import { Component, ViewEncapsulation } from '@angular/core';
const { shell } = require('electron');
@Component({
  selector: 'body',
  templateUrl: 'app.view.pug',
  styleUrls: ['app.style.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {

  displayOptions: boolean;

  constructor() {
  }

  showOptions(): void {
    this.displayOptions = !this.displayOptions;
  }

  openUrl(url: string) {
    shell.openExternal(url);
  }
}
