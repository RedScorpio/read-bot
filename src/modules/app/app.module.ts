import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './component/app.component';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { OptionsComponent } from '../options/component/options.component';
import { SlackComponent } from '../slack/component/slack.component';

@NgModule({
  imports: [
    BrowserModule, FormsModule, HttpModule
  ],
  declarations: [
    AppComponent, OptionsComponent, SlackComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
