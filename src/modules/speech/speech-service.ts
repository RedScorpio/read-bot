import { storage, Prefs } from '../../core/storage';
const AWS = require('aws-sdk');
const audioContext = new AudioContext();

export class Voice {
  name: string;
  language: string;

  constructor(voice) {
    this.name = voice.Id;
    this.language = voice.LanguageName;
  }
}

export class Speech {

  private static singleton: Speech|null = null;

  private polly;
  private voices: Voice[]|null = null;

  private constructor() {
    this.updateCredentials();
  }

  public static get(): Speech {
    if (Speech.singleton === null) {
      Speech.singleton = new Speech();
    }
    return Speech.singleton;
  }

  public updateCredentials(): void {
    AWS.config.update({
      accessKeyId: storage.getItem(Prefs.AmazonKeyId),
      secretAccessKey: storage.getItem(Prefs.AmazonSecret),
      region: storage.getItem(Prefs.AmazonRegion)
    });
    this.polly = new AWS.Polly();
  }

  public getVoices(): Promise<Voice[]> {
    return new Promise((resolve, reject) => {
      if (this.voices !== null) {
        return resolve(this.voices);
      }
      this.polly.describeVoices({}, (err, data) => {
        if (err) return reject(err);
        resolve(this.voices = data.Voices.map(voice => new Voice(voice)));
      });
    });
  }

  public synthesize(text: string): Promise<void> {
    return new Promise((resolve, reject) => {
      this.polly.synthesizeSpeech({
        OutputFormat: 'mp3',
        Text: text,
        VoiceId: storage.getItem(Prefs.Voice),
      }, function (err, data) {
        if (err) return reject(err);
        resolve(data.AudioStream);
      });
    }).then(stream => {
      this.playStream(stream);
    });
  }

  private playStream(stream) {
    const arrayBuffer = new ArrayBuffer(stream.length);
    const bufferView = new Uint8Array(arrayBuffer);
    for (let i = 0; i < stream.length; i++) {
      bufferView[i] = stream[i];
    }

    audioContext.decodeAudioData(arrayBuffer, buffer => {
      let source = audioContext.createBufferSource();
      source.buffer = buffer;
      source.connect(audioContext.destination);
      source.start(0);
    });
  }

}
