import { Component, OnInit } from '@angular/core';
import { storage, Prefs } from '../../../core/storage';
import { Speech, Voice } from '../../speech/speech-service';
import { SlackClient } from '../../slack/client/slack-service';
const { dialog } = require('electron').remote;

@Component({
  selector: 'optionsComponent',
  templateUrl: 'options.view.pug',
  styleUrls: ['options.style.scss']
})
export class OptionsComponent implements OnInit {

  slackClient:SlackClient = SlackClient.get();
  speech: Speech = Speech.get();
  voices: Voice[];
  voicesLoading: boolean;

  ngOnInit(): void {
    this.loadVoices();
  }

  loadVoices() {
    if (this.voicesLoading)return;
    this.voicesLoading = true;
    this.speech.getVoices()
      .then(voices => {
        this.voices = voices;
        this.voicesLoading = false;
      })
      .catch(err => {
        dialog.showErrorBox('Voices Error', 'Could not get list of voices: ' + err.message);
        this.voicesLoading = false;
      });
  }

  get autoConnect(): boolean {
    return 'true' === storage.getItem(Prefs.AutoConnect);
  }

  set autoConnect(value: boolean) {
    storage.setItem(Prefs.AutoConnect, '' + value);
  }

  get slackName(): string {
    return storage.getItem(Prefs.SlackName);
  }

  set slackName(value: string) {
    storage.setItem(Prefs.SlackName, value);
    this.slackClient.refreshChosenUser();
  }

  get slackToken(): string {
    return storage.getItem(Prefs.SlackToken);
  }

  set slackToken(value: string) {
    storage.setItem(Prefs.SlackToken, value);
    this.slackClient.disconnect();
  }

  get amazonId(): string {
    return storage.getItem(Prefs.AmazonKeyId);
  }

  set amazonId(value: string) {
    storage.setItem(Prefs.AmazonKeyId, value);
    this.speech.updateCredentials();
  }

  get amazonSecret(): string {
    return storage.getItem(Prefs.AmazonSecret);
  }

  set amazonSecret(value: string) {
    storage.setItem(Prefs.AmazonSecret, value);
    this.speech.updateCredentials();
  }

  get amazonRegion(): string {
    return storage.getItem(Prefs.AmazonRegion);
  }

  set amazonRegion(value: string) {
    storage.setItem(Prefs.AmazonRegion, value);
    this.speech.updateCredentials();
  }

  get voice(): string {
    return storage.getItem(Prefs.Voice);
  }

  set voice(value: string) {
    storage.setItem(Prefs.Voice, value);
  }
}
