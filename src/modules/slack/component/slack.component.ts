import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { SlackClient } from '../client/slack-service';
import { SlackMessage } from '../client/slack-message';
import { Subscription, Observable } from 'rxjs';
import { SlackUser } from '../client/slack-user';
import { formatSimpleDate } from '../../../core/utils';
import { Speech } from '../../speech/speech-service';
import { storage, Prefs } from '../../../core/storage';
const { dialog } = require('electron').remote;

@Component({
  selector: 'slackComponent',
  templateUrl: 'slack.view.pug',
  styleUrls: ['slack.style.scss']
})
export class SlackComponent implements OnInit, OnDestroy {

  private messagesSubscription: Subscription;
  private usersSubscription: Subscription;
  private timerSubscription: Subscription;

  speech: Speech;
  client: SlackClient;
  messages: SlackMessage[];
  users: SlackUser[];

  constructor(private changeDetector: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.client = SlackClient.get();
    this.speech = Speech.get();
    this.timerSubscription = Observable.timer(1000, 1000).subscribe(() => {
      this.changeDetector.detectChanges();
    });
    this.messagesSubscription = this.client.messagesObservable.subscribe(messages => {
      this.messages = messages;
      this.changeDetector.detectChanges();
      if (this.messages.length > 0) {
        const lastMessage = messages[messages.length - 1];
        this.speech.synthesize(lastMessage.speechText)
          .catch(err => {
            dialog.showErrorBox('Speech Error', err.message);
          });
      }
    });
    this.usersSubscription = this.client.usersObservable.subscribe(users => {
      this.users = users;
      this.changeDetector.detectChanges();
    });
    if ('true' === storage.getItem(Prefs.AutoConnect)) {
      this.connect();
    }
  }

  connect() {
    this.client.connect()
      .catch(err => {
        dialog.showErrorBox('Slack Error', err.message);
      });
  }

  ngOnDestroy(): void {
    this.messagesSubscription.unsubscribe();
    this.usersSubscription.unsubscribe();
    this.timerSubscription.unsubscribe();
  }

  formatDate(date: Date): string {
    return formatSimpleDate(date);
  }

}
