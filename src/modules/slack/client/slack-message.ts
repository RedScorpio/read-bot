import { SlackUser } from './slack-user';

export class SlackMessage {
  public text: string;
  public htmlText: string;
  public speechText: string;
  public date: Date;
  public sender: SlackUser;
  public edited: boolean = false;

  constructor(message, users: SlackUser[]) {
    if (message.message) {
      message = message.message;
      this.edited = true;
    }
    this.date = new Date(message.ts * 1000);
    this.text = message.text;
    this.htmlText = this.text;
    this.speechText = this.text;
    for (const user of users) {
      this.htmlText = this.htmlText.replace(new RegExp(user.idReference, 'g'), `<i>@${user.name}</i>`);
      this.speechText = this.speechText.replace(new RegExp(user.idReference, 'g'), user.realName || user.name);
      if (!this.sender && message.user === user.id) {
        this.sender = user;
      }
    }
  }

}