import { storage, Prefs } from '../../../core/storage';
import { SlackMessage } from './slack-message';
import { SlackUser } from './slack-user';
import { Property } from '../../../core/property';

const SlackAPI = (() => {
  try {
    return require('@slack/client');
  } catch (ignored) {
    // Fallback: Slack can't be run directly from browser
    return require('electron').remote.require('@slack/client');
  }
})();

const RtmClient = SlackAPI.RtmClient,
  WebClient = SlackAPI.WebClient,
  RTM_EVENTS = SlackAPI.RTM_EVENTS,
  CLIENT_EVENTS = SlackAPI.CLIENT_EVENTS,
  MemoryDataStore = SlackAPI.MemoryDataStore;

export class SlackClient {

  private static singleton: SlackClient|null = null;

  private rtmClient: any = null;
  private webClient: any = null;

  private messages: SlackMessage[] = [];
  private users: SlackUser[] = [];
  private chosenUser: SlackUser|null = null;

  public readonly messagesObservable: Property<SlackMessage[]> = new Property();
  public readonly usersObservable: Property<SlackUser[]> = new Property();

  private constructor() {
  }

  /**
   * Used for testing.
   */
  public static newInstance(): SlackClient {
    return new SlackClient();
  }

  public static get(): SlackClient {
    if (SlackClient.singleton === null) {
      SlackClient.singleton = new SlackClient();
    }
    return SlackClient.singleton;
  }

  public connect(): Promise<void> {
    const token = storage.getItem(Prefs.SlackToken);
    this.rtmClient = new RtmClient(token, {
      logLevel: 'error',
      dataStore: new MemoryDataStore(),
      autoReconnect: true
    });
    this.webClient = new WebClient(token);

    // Listen for messages only after user list's been downloaded
    return this.refreshUsers()
      .then(() => new Promise<void>((resolve, reject) => {
        this.rtmClient.on(CLIENT_EVENTS.RTM.AUTHENTICATED, () => resolve());
        this.rtmClient.on(RTM_EVENTS.MESSAGE, message => this.onMessageReceived(message));
        this.rtmClient.start();
      }));
  }

  public refreshUsers(): Promise<void> {
    return this.webClient.users.list().then(result => this.onUsersReceived(result));
  }

  public refreshChosenUser(): void {
    const name = storage.getItem(Prefs.SlackName);
    this.chosenUser = null;
    for (const user of this.users) {
      if (user.name === name || user.realName === name) {
        this.chosenUser = user;
        break;
      }
    }
  }

  onMessageReceived(message): void {
    if (!message || !message.text) return;
    const newMessage = new SlackMessage(message, this.users);
    if (!this.chosenUser || newMessage.text.indexOf(this.chosenUser.idReference) >= 0) {
      this.messages.push(newMessage);
      this.messagesObservable.next(this.messages);
    }
  }

  onUsersReceived(users): void {
    this.users = users.members.map(member => new SlackUser(member));
    this.refreshChosenUser();
    this.usersObservable.next(this.users);
  }

  public disconnect(): void {
    if (!this.connected) return;
    this.rtmClient.disconnect();
    this.rtmClient = null;
    this.webClient = null;
    this.messages = [];
    this.messagesObservable.next(this.messages);
    this.users = [];
    this.usersObservable.next(this.users);
  }

  public get connected(): boolean {
    return this.rtmClient != null;
  }

}
