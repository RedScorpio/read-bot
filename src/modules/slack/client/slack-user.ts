export class SlackUser {
  public id: string;
  public name: string;
  public realName: string;
  public imageUrl: string;

  constructor(user) {
    this.id = user.id;
    this.name = user.name;
    this.realName = user.real_name || this.name;
    this.imageUrl = user.profile ? user.profile.image_72 : '';
  }

  public get idReference() {
    return `<@${this.id}>`;
  }
}