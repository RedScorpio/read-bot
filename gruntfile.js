'use strict';

/**
 * Module dependencies.
 */
const fs = require('fs'),
  path = require('path'),
  _ = require('lodash'),
  assets = require('./assets.json');

module.exports = function (grunt) {

  const electronDefaults = {
    name: 'ReadBot',
    dir: assets.appDir,
    out: assets.buildDir + 'out',
    electronVersion: '1.6.1',
    arch: 'x64'
  };

  const electronConfig = {
    darwinBuild: {
      options: _.defaults({
        platform: 'darwin',
        icon: 'resources/icons/mac.icns'
      }, electronDefaults)
    },
    linuxBuild: {
      options: _.defaults({
        platform: 'linux'
      }, electronDefaults)
    },
    win32Build: {
      options: _.defaults({
        platform: 'win32',
        icon: 'resources/icons/windows/ico'
      }, electronDefaults)
    }
  };
  electronConfig.default = electronConfig[process.platform + 'Build'];

  // Project Configuration
  grunt.initConfig({
    clean: [assets.buildDir, '.tscache'],
    copy: {
      tests: {
        expand: true,
        cwd: assets.sourceDir,
        src: ['./**/*.json'],
        dest: assets.testsDir
      },
      app: {
        files: [
          {
            expand: true,
            src: [
              'package.json',
              'node_modules/**',
              'resources/**'
            ],
            dest: assets.appDir
          },
          {
            expand: true,
            nonull: true,
            cwd: 'src/core',
            src: ['index.html', 'main.js'],
            dest: assets.appDir
          }
        ],
      }
    },
    electron: electronConfig,
    jshint: {
      all: {
        src: [assets.sourceDir + '**/*.js'],
        options: {
          reporter: require('jshint-stylish'),
          jshintrc: true,
          node: true,
          mocha: true
        }
      }
    },
    eslint: {
      options: {},
      target: [assets.sourceDir + '**/*.js']
    },
    tslint: {
      options: {
        configuration: './tslint.json',
        force: true
      },
      files: {
        src: [assets.sourceDir + '**/*.ts']
      }
    },
    ts: {
      options: {
        compiler: './node_modules/typescript/bin/tsc'
      },
      tests: {
        tsconfig: assets.sourceDir + 'tests/tsconfig.json'
      }
    },
    webpack: {
      options: {
        failOnError: true
      },
      dev: require('./webpack/development'),
      test: require('./webpack/test'),
      prod: require('./webpack/production')
    },
    mochaTest: {
      src: assets.testsGlob,
      options: {
        reporter: 'spec',
        timeout: 10000
      }
    },
  });

  require('load-grunt-tasks')(grunt);


  // Lint JavaScript files.
  grunt.registerTask('lint', ['jshint', 'eslint', 'tslint']);

  // Build configs
  grunt.registerTask('build', ['lint', 'copy:app', 'webpack:dev', 'setRunPath']);

  grunt.registerTask('buildTest', ['lint', 'copy:app', 'webpack:test', 'copy:tests', 'ts:tests', 'setRunPath']);

  grunt.registerTask('buildProd', ['lint', 'copy:app', 'webpack:prod', 'setRunPath']);

  // Setting executable run path
  grunt.registerTask('setRunPath', () => {
    const app = grunt.config.get('electron').default.options;
    const appDirectory = path.join(app.out, app.name + '-' + app.platform + '-' + app.arch);
    let runPath = null;
    switch (process.platform) {
      case 'darwin':
        runPath = path.join(appDirectory, app.name + '.app', 'Contents/MacOS', app.name);
        break;
      case 'linux':
        runPath = path.join(appDirectory, app.name);
        break;
      case 'win32':
        runPath = path.join(appDirectory, app.name + '.exe');
        break;
    }
    process.env.RUN_PATH = runPath;
  });

  // Run app on macOS
  grunt.registerTask('run', () => {
    const exec = require('child_process').execSync,
      runPath = process.env.RUN_PATH;
    if (!runPath) {
      console.log('Missing run path.');
      return 1;
    }
    return exec(runPath);
  });

  // Run tests
  grunt.registerTask('test', ['clean', 'buildTest', 'electron:default', 'mochaTest']);

  // Build all for prod
  grunt.registerTask('production', ['clean', 'buildProd', 'electron:darwinBuild', 'electron:linuxBuild', 'electron:win32Build']);

  // Build & launch
  grunt.registerTask('default', ['clean', 'build', 'electron:default', 'run']);

};
