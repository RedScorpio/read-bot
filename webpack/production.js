'use strict';
const webpack = require('webpack');
const webpackMerge = require('webpack-merge');

//noinspection JSUnresolvedFunction
module.exports = webpackMerge(require('./default'), {
  plugins: [
    new webpack.DefinePlugin({
      'process.env.PRODUCTION_MODE': true
    }),
    new webpack.optimize.UglifyJsPlugin({
      beautify: false,
      mangle: true,
      compress: { warnings: false, screw_ie8: true },
      comments: false
    })
  ]
});
