'use strict';
const webpack = require('webpack');
const assets = require('../assets.json');
const TsConfigPathsPlugin = require('awesome-typescript-loader').TsConfigPathsPlugin;

const root = assets.sourceDir;

//noinspection JSUnresolvedFunction
module.exports = {
  entry: {
    polyfills: root + 'core/polyfills.ts',
    vendor: root + 'core/vendor.ts',
    app: root + 'core/app.ts'
  },
  output: {
    path: assets.appDir,
    filename: '[name].js',
  },
  resolve: {
    extensions: ['.ts', '.js'],
    plugins: [
      new TsConfigPathsPlugin({ tsconfig: './tsconfig.json' })
    ]
  },
  stats: {
    colors: true,
    modules: true,
    reasons: true,
    errorDetails: true
  },
  module: {
    rules: [
      {
        test: /\.(pug|jade)$/,
        loader: 'pug-html-loader'
      },
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        loaders: ['raw-loader', 'sass-loader']
      },
      {
        test: /\.html$/,
        loader: 'html-loader'
      },
      {
        test: /\.ts$/,
        loaders: ['awesome-typescript-loader?configFileName=tsconfig.json', 'angular2-template-loader']
      }
    ]
  },
  plugins: [
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.optimize.CommonsChunkPlugin({
      name: ['app', 'vendor', 'polyfills']
    })
  ],
  progress: false,
  target: 'electron-renderer'
};
