# ReadBot

An Electron-based desktop Slack Bot that displays and reads out messages. The app's made using Grunt, Angular 2, TypeScript, Webpack, SASS, Pug, AWS Polly and Slack API.

All options can be configured either in GUI or via environmental variables:

* `SlackToken` - Slack API token that can be generated through team settings.
* `SlackName` - Name of a user which is a filter for incoming messages. Bot accepts all messages if empty.
* `AmazonKeyId` - Amazon API Key Identifier (for IAM account with Polly read-only privileges).
* `AmazonSecret` - Secret for Amazon API Key.
* `AmazonRegion` - Region for Amazon instance being used.
* `Voice` - A voice of a bot. The updated list is displayed in app Settings. Try `Joanna` for US English.
* `AutoConnect` - Boolean, determines whether bot should connect immediately after launch.

## Looks

![Messages view](screenshots/screen-1.png)
![Settings view](screenshots/screen-2.png)

## Cool stuff
* Linters and TypeScript that help not to make another PR introducing 40 potentially dangerous typos.
* Displaying user list and their images in messages.
* A humble set of tests that is actually passing and checking something.
* Fairly organized project structure.
* Settings that can be saved between launches. `localStorage` may be replaced with more fancy storage engine... Encryption, anyone?

## Known issues
* Build requires `node_modules` contents to be copied into a directory which is later packaged using Electron.
* App doesn't queue messages and may say multiple messages at once.
* No quit button. Use CMD/CTRL+Q.
* Bot doesn't run in background. A window has to be opened to listen for messages. TODO: Implement that with Electron's IPC.
* Probably missing Slack error events handling (didn't notice them in a quick API skim).

## Build, test & run

To build this project from source, you'll need [Node.js](http://nodejs.org/). Then run Terminal in the project directory and type instructions below:

```bash
npm install # Downloads all required packages.
npm start # Builds project using development environment for your platform, by executing default Grunt task. Waits until the app ends. It's slower than production version.
```

Other commands:

* `grunt build` - Prepares development environment app, but does not package Electron app
* `grunt buildTest` - Prepares test environment app, but does not package Electron app
* `grunt buildProd` - Prepares production environment app, but does not package Electron app
* `grunt clean` - Removes everything from build directory and cleans TypeScript cache.
* `grunt` - Cleans & builds development environment app .
* `grunt test` - Cleans & builds test app and executes mocha tests on your platform.
* `grunt production` - Cleans & builds production app and packages it for all platforms. Wine is required on Linux and macOS to build for Windows.
